﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MTData.Transport.Service.Listener.GatewayServiceBL.Classes
{
    [DataContract]
    public class QueuedDownload
    {
        #region Private Fields
        private int id;
        private int definitionID;
        private int fleetID;
        private string fleetName;
        private int vehicleId;
        private string vehicleDisplayName;
        private DateTime timeCreated;
        private bool forceUpdate;
        private bool forceLogout;
        private bool active;
        private DateTime startDate;
        private DateTime expiryDate;
        private int createdUserID;
        private string createdUserName;
        private int abortTimeout;
        private bool continueWithoutRelogin;
        private string smtpEmailAddress;
        private string smtpServerAddress;
        private DateTime headerRequestTime;
        private DateTime downloadVerifyTime;
        private int totalSegment;
        private int sentSegment;
        private bool completed;
        private DateTime lastModified;
        private string downloadType;
        private int retryCount;
        private int retryInterval;
        private int segmentSize;
        private bool ignitionOnRequired;
        private ListenerDownloadType listenerDownloadType;
        private Byte[] fileData;

        private bool fromService;
        #endregion

        #region Constructor
        public QueuedDownload()
        {
            this.fromService = false;
        }

        public QueuedDownload(System.Data.SqlClient.SqlDataReader sdr, bool fromService)
        {
            this.fromService = fromService;
            this.id = sdr.GetInt32(sdr.GetOrdinal("id"));
            this.definitionID = sdr.GetInt32(sdr.GetOrdinal("DefinitionID"));
            this.fleetID = sdr.GetInt32(sdr.GetOrdinal("fleetID"));
            this.fleetName = sdr.GetString(sdr.GetOrdinal("fleetName"));
            this.vehicleId = sdr.GetInt32(sdr.GetOrdinal("VehicleID"));
            this.vehicleDisplayName = sdr.GetString(sdr.GetOrdinal("VehicleDisplayName"));
            this.timeCreated = sdr.GetDateTime(sdr.GetOrdinal("TimeCreated"));
            this.forceUpdate = sdr.GetBoolean(sdr.GetOrdinal("ForceUpdate"));
            this.forceLogout = sdr.GetBoolean(sdr.GetOrdinal("ForceLogout"));
            this.active = sdr.GetBoolean(sdr.GetOrdinal("Active"));
            this.startDate = sdr.GetDateTime(sdr.GetOrdinal("StartDate"));
            this.expiryDate = sdr.GetDateTime(sdr.GetOrdinal("ExpiryDate"));
            this.createdUserID = sdr.GetInt32(sdr.GetOrdinal("CreatedUserID"));
            this.createdUserName = sdr.GetString(sdr.GetOrdinal("UserName"));
            this.abortTimeout = sdr.GetInt32(sdr.GetOrdinal("AbortTimeout"));
            this.continueWithoutRelogin = sdr.GetBoolean(sdr.GetOrdinal("ContinueWithoutRelogin"));
            this.smtpEmailAddress = sdr.GetString(sdr.GetOrdinal("SMTPEmailAddress"));
            this.headerRequestTime = sdr.IsDBNull(sdr.GetOrdinal("HeaderRequestTime")) ? DateTime.MinValue : sdr.GetDateTime(sdr.GetOrdinal("HeaderRequestTime"));
            this.downloadVerifyTime = sdr.IsDBNull(sdr.GetOrdinal("DownloadVerifyTime")) ? DateTime.MinValue : sdr.GetDateTime(sdr.GetOrdinal("DownloadVerifyTime"));
            this.totalSegment = sdr.IsDBNull(sdr.GetOrdinal("TotalSegment")) ? 0 : sdr.GetInt32(sdr.GetOrdinal("TotalSegment"));
            this.sentSegment = sdr.IsDBNull(sdr.GetOrdinal("SentSegment")) ? 0 : sdr.GetInt32(sdr.GetOrdinal("SentSegment"));
            this.completed = sdr.IsDBNull(sdr.GetOrdinal("Completed")) ? false : sdr.GetBoolean(sdr.GetOrdinal("Completed"));
            this.lastModified = sdr.IsDBNull(sdr.GetOrdinal("LastModified")) ? DateTime.MinValue : sdr.GetDateTime(sdr.GetOrdinal("LastModified"));
            this.downloadType = sdr.GetString(sdr.GetOrdinal("DownloadName"));
            this.retryCount = sdr.GetInt32(sdr.GetOrdinal("RetryCount"));
            this.retryInterval = sdr.GetInt32(sdr.GetOrdinal("RetryInterval"));
            this.segmentSize = sdr.GetInt32(sdr.GetOrdinal("SegmentSize"));
            this.ignitionOnRequired = sdr.GetBoolean(sdr.GetOrdinal("IgnitionOnRequired"));
            this.listenerDownloadType = (ListenerDownloadType)Enum.Parse(typeof(ListenerDownloadType), sdr.GetInt32(sdr.GetOrdinal("ListenerDownloadType")).ToString());
        }
        #endregion

        #region Public Events/Delegates

        #endregion

        #region Public Properties
        [DataMember]
        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        [DataMember]
        public int DefinitionID
        {
            get { return this.definitionID; }
            set { this.definitionID = value; }
        }

        [DataMember]
        public int FleetID
        {
            get { return this.fleetID; }
            set { this.fleetID = value; }
        }

        [DataMember]
        public string FleetName
        {
            get { return this.fleetName; }
            set { this.fleetName = value; }
        }

        [DataMember]
        public int VehicleId
        {
            get { return this.vehicleId; }
            set { this.vehicleId = value; }
        }

        [DataMember]
        public string VehicleDisplayName
        {
            get { return this.vehicleDisplayName; }
            set { this.vehicleDisplayName = value; }
        }

        [DataMember]
        public DateTime TimeCreated
        {
            get { return this.timeCreated; }
            set { this.timeCreated = value; }
        }

        [DataMember]
        public bool ForceUpdate
        {
            get { return this.forceUpdate; }
            set { this.forceUpdate = value; }
        }

        [DataMember]
        public bool ForceLogout
        {
            get { return this.forceLogout; }
            set { this.forceLogout = value; }
        }

        [DataMember]
        public bool Active
        {
            get { return this.active; }
            set { this.active = value; }
        }

        [DataMember]
        public DateTime StartDate
        {
            get { return this.startDate; }
            set { this.startDate = value; }
        }

        [DataMember]
        public DateTime ExpiryDate
        {
            get { return this.expiryDate; }
            set { this.expiryDate = value; }
        }

        [DataMember]
        public int CreatedUserID
        {
            get { return this.createdUserID; }
            set { this.createdUserID = value; }
        }

        [DataMember]
        public string CreatedUserName
        {
            get { return this.createdUserName; }
            set { this.createdUserName = value; }
        }

        [DataMember]
        public int AbortTimeout
        {
            get { return this.abortTimeout; }
            set { this.abortTimeout = value; }
        }

        [DataMember]
        public bool ContinueWithoutRelogin
        {
            get { return this.continueWithoutRelogin; }
            set { this.continueWithoutRelogin = value; }
        }

        [DataMember]
        public string SMTPEmailAddress
        {
            get { return this.smtpEmailAddress; }
            set { this.smtpEmailAddress = value; }
        }

        [DataMember]
        public string SMTPServerAddress
        {
            get { return this.smtpServerAddress; }
            set { this.smtpServerAddress = value; }
        }

        [DataMember]
        public DateTime HeaderRequestTime
        {
            get { return this.headerRequestTime; }
            set { this.headerRequestTime = value; }
        }

        [DataMember]
        public DateTime DownloadVerifyTime
        {
            get { return this.downloadVerifyTime; }
            set { this.downloadVerifyTime = value; }
        }

        [DataMember]
        public int TotalSegment
        {
            get { return this.totalSegment; }
            set { this.totalSegment = value; }
        }

        [DataMember]
        public int SentSegment
        {
            get { return this.sentSegment; }
            set { this.sentSegment = value; }
        }

        [DataMember]
        public bool Completed
        {
            get { return this.completed; }
            set { this.completed = value; }
        }

        [DataMember]
        public DateTime LastModified
        {
            get { return this.lastModified; }
            set { this.lastModified = value; }
        }

        [DataMember]
        public string DownloadType
        {
            get { return this.downloadType; }
            set { this.downloadType = value; }
        }

        [DataMember]
        public string DownloadStatus
        {
            get
            {
                if (!this.active && ((this.sentSegment != this.totalSegment) || (this.sentSegment == this.totalSegment && this.sentSegment == 0 && this.totalSegment == 0)))
                {
                    return "Aborted";
                }
                else if (!this.active && (this.sentSegment == this.totalSegment))
                {
                    return "Completed";
                }
                else if (this.sentSegment == 0)
                {
                    return "Pending";
                }
                else if (this.totalSegment < this.sentSegment)
                {
                    return "In Progress";
                }
                else
                {
                    return "Unknown";
                }
            }

            set
            {
            }
        }

        [DataMember]
        public int DownloadStatusId
        {
            get
            {
                if (!this.active && ((this.sentSegment != this.totalSegment) || (this.sentSegment == this.totalSegment && this.sentSegment == 0 && this.totalSegment == 0)))
                {
                    return 3;
                }
                else if (!this.active && (this.sentSegment == this.totalSegment))
                {
                    return 4;
                }
                else if (this.sentSegment == 0)
                {
                    return 0;
                }
                else if (this.totalSegment < this.sentSegment)
                {
                    return 1;
                }
                else
                {
                    return 1;
                }
            }

            set
            {
            }
        }

        [DataMember]
        public int RetryCount
        {
            get { return this.retryCount; }
            set { this.retryCount = value; }
        }

        [DataMember]
        public int RetryInterval
        {
            get { return this.retryInterval; }
            set { this.retryInterval = value; }
        }

        [DataMember]
        public int SegmentSize
        {
            get { return this.segmentSize; }
            set { this.segmentSize = value; }
        }

        [DataMember]
        public bool IgnitionOnRequired
        {
            get { return this.ignitionOnRequired; }
            set { this.ignitionOnRequired = value; }
        }
        
        [DataMember]
        public ListenerDownloadType ListenerDownloadType
        {
            get { return this.listenerDownloadType; }
            set { this.listenerDownloadType = value; }
        }

        [DataMember]
        public Byte[] FileData
        {
            get
            {
                if (this.fileData == null && this.fromService)
                {
                    this.LoadFile();
                }
                
                return this.fileData;
            }

            set
            {
                this.fileData = value;
            }
        }
        #endregion

        #region Methods
        private void LoadFile()
        {
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectString"]);

            string sql = "GetDownloadBinaryData";

            System.Data.SqlClient.SqlCommand comm = new System.Data.SqlClient.SqlCommand(sql);

            comm.Parameters.AddWithValue("DispatchID", this.definitionID);

            comm.CommandType = System.Data.CommandType.StoredProcedure;
            comm.Connection = conn;

            if (conn.State != System.Data.ConnectionState.Open)
            {
                conn.Open();
            }

            System.Data.SqlClient.SqlDataReader dr = comm.ExecuteReader();

            if (dr.HasRows)
            {
                dr.Read();

                this.fileData = dr.GetSqlBytes(dr.GetOrdinal("fileData")).Buffer;
            }
        }
        #endregion

        #region Override Methods

        #endregion

        #region Interface Methods
        #endregion
    }
}
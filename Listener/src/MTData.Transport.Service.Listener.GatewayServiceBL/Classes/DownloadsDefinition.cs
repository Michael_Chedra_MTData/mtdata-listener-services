﻿using System;
using System.Runtime.Serialization;

namespace MTData.Transport.Service.Listener.GatewayServiceBL.Classes
{
    public enum DownloadsUpdateType
    {
        All = -1,
        WinCE = 1,
        NEC = 2,
        Firmware5040 = 3,
        Firmware5010 = 4,
        ShellConfig = 5,
        MapDll = 6,
        Firmware4000 = 7,
        Firmware30xx = 8,
        Firmware5080 = 9,
        Firmware1035_1039 = 10,
        SwiftTalonConfig = 11,
        Swift = 12,
        Talon = 13,
        Talon3rdPartyApp = 14
    }

    [DataContract]
    public class DownloadsDefinition
    {
        #region Constructor

        public DownloadsDefinition(System.Data.SqlClient.SqlDataReader sdr)
        {
            DefinitionID = sdr.GetInt32(sdr.GetOrdinal("DefinitionID"));
            UpdateName = sdr.GetString(sdr.GetOrdinal("UpdateName"));
            VersionMajor = sdr.GetInt32(sdr.GetOrdinal("VersionMajor"));
            VersionMinor = sdr.GetInt32(sdr.GetOrdinal("VersionMinor"));

            // Attempt to parse the update type
            try
            {
                int updateTypeId = sdr.GetInt32(sdr.GetOrdinal("UpdateTypeId"));
                UpdateType = Enum.IsDefined(typeof(DownloadsUpdateType), updateTypeId) ? (DownloadsUpdateType)updateTypeId : DownloadsUpdateType.All;
            }
            catch (Exception)
            {
                UpdateType = DownloadsUpdateType.All;
            }
        }

        #endregion

        #region Public Properties

        [DataMember]
        public int DefinitionID
        {
            get; set;
        }

        [DataMember]
        public string UpdateName
        {
            get; set;
        }

        [DataMember]
        public int VersionMajor
        {
            get; set;
        }

        [DataMember]
        public int VersionMinor
        {
            get; set;
        }

        [DataMember]
        public DownloadsUpdateType UpdateType
        {
            get; set;
        }

        #endregion
    }
}

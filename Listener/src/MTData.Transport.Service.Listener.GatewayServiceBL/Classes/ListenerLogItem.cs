﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MTData.Transport.Service.Listener.GatewayServiceBL.Classes
{
    [DataContract]
    public class ListenerLogItem
    {
        #region Private Fields
        private int logID;
        private int fleetID;
        private string fleetName;
        private int vehicleID;
        private string vehicleDisplayName;
        private string listenerMessage;
        private string messageType;
        private LogDirection direction;
        private DateTime logDate;
        private string ipAddress;
        private string serialNumber;
        #endregion

        #region Constructor
        public ListenerLogItem(System.Data.SqlClient.SqlDataReader sdr)
        {
            this.logID = sdr.GetInt32(sdr.GetOrdinal("logID"));
            this.fleetID = sdr.GetInt32(sdr.GetOrdinal("fleetID"));
            this.fleetName = sdr.GetString(sdr.GetOrdinal("fleetName"));
            this.vehicleID = sdr.GetInt32(sdr.GetOrdinal("vehicleID"));
            this.vehicleDisplayName = sdr.GetString(sdr.GetOrdinal("DisplayName"));
            this.listenerMessage = sdr.GetString(sdr.GetOrdinal("listenerMessage"));
            this.messageType = sdr.GetString(sdr.GetOrdinal("messageType"));
            this.direction = (LogDirection)Enum.Parse(typeof(LogDirection), sdr.GetInt32(sdr.GetOrdinal("direction")).ToString());
            this.logDate = sdr.GetDateTime(sdr.GetOrdinal("logDate"));
            this.ipAddress = sdr.GetString(sdr.GetOrdinal("ipAddress"));

            try
            {
                this.serialNumber = sdr.GetInt64(sdr.GetOrdinal("serialNumber")).ToString();
            }
            catch (Exception)
            {
                this.serialNumber = "Unavailable";
            }
        }
        #endregion

        #region Public Events/Delegates
        #endregion

        #region Public Properties
        [DataMember]
        public int LogID
        {
            get { return this.logID; }
            set { this.logID = value; }
        }

        [DataMember]
        public int FleetID
        {
            get { return this.fleetID; }
            set { this.fleetID = value; }
        }

        [DataMember]
        public string FleetName
        {
            get { return this.fleetName; }
            set { this.fleetName = value; }
        }

        [DataMember]
        public int VehicleID
        {
            get { return this.vehicleID; }
            set { this.vehicleID = value; }
        }

        [DataMember]
        public string VehicleDisplayName
        {
            get { return this.vehicleDisplayName; }
            set { this.vehicleDisplayName = value; }
        }

        [DataMember]
        public string ListenerMessage
        {
            get { return this.listenerMessage; }
            set { this.listenerMessage = value; }
        }

        [DataMember]
        public string MessageType
        {
            get { return this.messageType; }
            set { this.messageType = value; }
        }
        
        [DataMember]
        public LogDirection Direction
        {
            get { return this.direction; }
            set { this.direction = value; }
        }

        [DataMember]
        public DateTime LogDate
        {
            get { return this.logDate; }
            set { this.logDate = value; }
        }

        [DataMember]
        public string IPAddress
        {
            get { return this.ipAddress; }
            set { this.ipAddress = value; }
        }

        [DataMember]
        public string SerialNumber
        {
            get { return this.serialNumber; }
            set { this.serialNumber = value; }
        }
        #endregion

        #region Methods

        #endregion

        #region Override Methods

        #endregion

        #region Interface Methods
        #endregion

    }
}

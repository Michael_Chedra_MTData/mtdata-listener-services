﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using log4net;
using MTData.Common.Threading;
using MTData.Transport.Application.Communication;
using MTData.Transport.Application.Communication.MsmqInterface;

namespace MTData.Transport.Service.Listener.GatewayServiceBL
{
    public class CurrentLogItems : IDisposable
    {
        private static readonly object _padlock = new object();
        private static CurrentLogItems _instance;

        private readonly object _listSync = new object();

        private ILog _log = LogManager.GetLogger(typeof(CurrentLogItems));
        
        private ListenerLogToMsmq _logQueue;
        private EnhancedThread _processMsmqThread;
        private List<ListenerLogItem> _items;
        private int _keepItemsInMemoryForMins;
        private DateTime _nextTimeCheck;

        #region constructor
        private CurrentLogItems()
        {
            _items = new List<ListenerLogItem>();
            _keepItemsInMemoryForMins = Convert.ToInt32(ConfigurationManager.AppSettings["KeepItemsInMemoryForMins"]);
        }
        #endregion

        #region singleton
        public static CurrentLogItems Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_padlock)
                    {
                        if (_instance == null)
                        {
                            _instance = new CurrentLogItems();
                        }
                    }
                }

                return _instance;
            }
        }
        #endregion

        #region implement IDisposable
        public void Dispose()
        {
            if (_processMsmqThread != null)
            {
                _processMsmqThread.Stop();
                int timer = 200;
                while (_processMsmqThread.State != ThreadState.Stopped && timer > 0)
                {
                    timer--;
                    Thread.Sleep(100);
                }
            }

            _logQueue.Dispose();
        }
        #endregion

        #region public methods
        public void Intialise()
        {
            // load the meesage queue
            try
            {
                if (_logQueue == null)
                {
                    _logQueue = (ListenerLogToMsmq)ConfigurationManager.GetSection("ListenerLogToMsmq");
                }
            }
            catch (Exception exp)
            {
                _log.Error("Error opening listener log message queue", exp);
            }

            // start a thread that will read the queue
            if (_processMsmqThread == null)
            {
                _processMsmqThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ProcessListenerLogMsmq), null);
                _processMsmqThread.Name = "Load Listener Log Messages from MSMQ";
                _processMsmqThread.Start();
            }
        }

        public List<ListenerLogItem> GetListenerLogs(DateTime lastTime)
        {
            lock (_listSync)
            {
                return _items.FindAll(e => (e.LogDate > lastTime));
            }
        }

        public List<ListenerLogItem> GetListenerLogs(int fleetId, int vehicleId, DateTime lastTime)
        {
            lock (_listSync)
            {
                return _items.FindAll(e => (e.FleetID == fleetId && e.VehicleID == vehicleId && e.LogDate > lastTime));
            }
        }
        #endregion

        #region private methods
        private object ProcessListenerLogMsmq(EnhancedThread sender, object data)
        {
            _log.Info("ProcessListenerLogMsmq() thread started");
            try
            {
                // check to delete any items in 5 minutes time
                _nextTimeCheck = DateTime.Now.AddMinutes(5);
                _log.InfoFormat("next check at {0}", _nextTimeCheck.ToShortTimeString());

                while (!sender.Stopping)
                {
                    try
                    {
                        // read items from queue and add to the list
                        Dictionary<int, ListenerLogItem> lus = _logQueue.ReadFromQueues;
                        if (lus != null && lus.Keys.Count > 0)
                        {
                            lock (_listSync)
                            {
                                _items.AddRange(lus.Values);
                                _log.DebugFormat("Adding items to list, count {0}", _items.Count);
                            }
                        }
                        else
                        {
                            Thread.Sleep(100);
                        }

                        // check if any items need to be deleted from the list
                        if (DateTime.Now > _nextTimeCheck)
                        {
                            _nextTimeCheck = DateTime.Now.AddMinutes(5);

                            // remove all items older than KeepItemsInMemoryForMins from now
                            DateTime time = DateTime.UtcNow.AddMinutes(_keepItemsInMemoryForMins * -1);
                            lock (_listSync)
                            {
                                int count = _items.RemoveAll(e => (e.LogDate < time));
                                _log.InfoFormat("removing items from list, count {0}, removed {1}", _items.Count, count);
                            }

                            _log.InfoFormat("next check at {0}", _nextTimeCheck.ToShortTimeString());
                        }
                    }
                    catch (Exception exp)
                    {
                        _log.Error(exp.Message);
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
            }

            _log.Info("ProcessListenerLogMsmq() thread stopped");
            return null;
        }
        #endregion
    }
}

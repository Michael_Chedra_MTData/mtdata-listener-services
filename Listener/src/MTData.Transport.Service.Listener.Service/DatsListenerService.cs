#define SINGLE_THREADED

using System;
using System.Text;
using System.Threading;
using System.Diagnostics;
using MTData.Transport.Gateway.Packet;
using System.Configuration;
using MTData.Common.Network;
using log4net;
using MTData.Transport.Service.Listener.GatewayListener;
using MTData.Common.Database;
using MTData.Common.Utilities;

// DATSListener Service should be installed as user "NT AUTHORITY\NetworkService" (no password)
namespace MTData.Transport.Service.Listener.Service
{
    public class DATSListenerService : System.ServiceProcess.ServiceBase
    {
        private const string sClassName = "DATSListenerService.DATSListenerService.";
        private static ILog _log = LogManager.GetLogger(typeof(DATSListenerService));

        #region Variables
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        private string _serverTime_DateFormat = "";
        private IEmailHelper emailHelper;
        #endregion

        #region Auto generated Service Code

        public DATSListenerService(IEmailHelper emailHelper)
        {
            this.emailHelper = emailHelper;
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //check database version
            CheckDatabaseVersion checkDB = new CheckDatabaseVersion(System.Configuration.ConfigurationManager.AppSettings["ConnectString"]);
            DatabaseVersionErrorMessage errorMessage;
            if (checkDB.CheckDatabaseRequiredVersion(8, 2, 0, 1, out errorMessage) == false &&
                        errorMessage != null)
            {
                string emailServiceUrl = ConfigurationManager.AppSettings["EmailServiceUrl"];
                new EmailHelper(new EmailClient(emailServiceUrl)).SendDatabaseVersionErrorEmail(errorMessage);
            }
            try
            {
                _serverTime_DateFormat = System.Configuration.ConfigurationManager.AppSettings["ServerTime_DateFormat"];
            }
            catch (System.Exception)
            {
                _serverTime_DateFormat = "dd/MM/yyyy HH:mm:ss";
            }
            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings["DebugMode"] != null)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["DebugMode"].ToLower() == "true")
                        OnStart(null);
                }
            }
            catch (System.Exception)
            {
            }

        }

		// The main entry point for the process
		static void Main(IEmailClient emailClient)
		{
            log4net.Config.XmlConfigurator.Configure();
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
			System.ServiceProcess.ServiceBase[] ServicesToRun;

            // More than one user Service may run within the same process. To add
            // another service to this process, change the following line to
            // create a second service object. For example,
            //
            //   ServicesToRun = New System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};
            //
		    string emailServiceUrl = ConfigurationManager.AppSettings["EmailServiceUrl"];
            var emailHelper = new EmailHelper(new EmailClient(emailServiceUrl));
            ServicesToRun = new System.ServiceProcess.ServiceBase[] { new DATSListenerService(emailHelper) };

            System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = (Exception)e.ExceptionObject;
            string sMsg = "App Domain Error :\r\n" + ex.Message + "\r\n\r\nSource : \r\n" + ex.Source + "\r\n\r\nStack Trace : \r\n" + ex.StackTrace;
            EventLog el = new EventLog();
            el.Source = "MTData Listener Service";
            el.WriteEntry(sMsg, System.Diagnostics.EventLogEntryType.Error);
            el.Close();
        }
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // 
            // DATSListenerService
            // 
            this.ServiceName = "DATSListenerService";

        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            OnStop();
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// This is the listener that controls the Gateway functionality
        /// </summary>
        private GatewayAdministrator _listener = null;

        #endregion


        /// <summary>
        /// Set things in motion so your service can do its work.
        /// </summary>
        protected override void OnStart(string[] args)
        {

            _asyncStartThread = new Thread(new ThreadStart(PerformAsynStart));
            _asyncStartThread.Start();
        }

        private Thread _asyncStartThread = null;

        /// <summary>
        /// This method will be called asynchornously to ensure that the service start mechanism is not interrupted
        /// </summary>
        private void PerformAsynStart()
        {
            int messagePortNumber = 4000;
            try
            {
                string temp = ConfigurationManager.AppSettings["LogOutboundMessagesPort"];
                if (temp != null)
                    messagePortNumber = Convert.ToInt32(temp);

                //	The default is not to log outbound messages
                bool logOutboundMessages = false;
                temp = ConfigurationManager.AppSettings["LogOutboundMessages"];
                if (temp != null)
                    logOutboundMessages = (temp.ToLower() == "true");


                //	To ascertain if the prerequisites are available, check them in a loop..
                temp = ConfigurationManager.AppSettings["PreReqTimeout"];
                int preRequisitesTimeout = (temp != null) ? Convert.ToInt32(temp) : 30;

                temp = ConfigurationManager.AppSettings["PreReqMaxRetries"];
                int preRequisitesMaxRetries = (temp != null) ? Convert.ToInt32(temp) : 10;
                if (preRequisitesMaxRetries == 0)
                    preRequisitesMaxRetries = int.MaxValue;

                bool preRequisitesAvailable = false;
                _log.Info("Confirming Pre-Requisites");

                int preRequisiteCheckCount = 0;
                try
                {
                    //  confirm the prerequisites are present 	
                    while (!preRequisitesAvailable)
                    {
                        preRequisitesAvailable = GatewayAdministrator.ConfirmPreRequisites();
                        if (!preRequisitesAvailable)
                        {
                            preRequisiteCheckCount++;
                            if (preRequisiteCheckCount < preRequisitesMaxRetries)
                            {
                                _log.Info("Confirming Pre-Requisites - Pre-Requisites Unavailable");
                                EventLog.WriteEntry("DATSListenerService", "Pre-Requisites Unavailable", EventLogEntryType.Warning);
                                Thread.Sleep(preRequisitesTimeout * 1000);
                            }
                            else
                            {

                                _log.Info("Confirming Pre-Requisites - Pre-Requisites Unavailable - Terminating Attempts");
                                EventLog.WriteEntry("DATSListenerService", "Pre-Requisites Unavailable - Terminating Attempts", EventLogEntryType.Error);
                                break;
                            }
                        }
                        else
                            break;
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(sClassName + "PerformAsynStart()", ex);
                    EventLog.WriteEntry("DATSListenerService", string.Format("Pre-Requisites Error : {0} : {1}", ex.Message, ex.StackTrace), EventLogEntryType.Error);
                }
                if (preRequisitesAvailable)
                {
                    _listener = new GatewayAdministrator();
                    string emailServiceUrl = ConfigurationManager.AppSettings["EmailServiceUrl"];
                    string error = _listener.Start(new EmailHelper(new EmailClient(emailServiceUrl)));
                    if (error != null)
                    {
                        //throw new Exception("An Error Occurred during initialisation\r\n" + error + "\r\nPlease contact Support");
                        _log.Info("An Error Occurred during initialisation\r\n" + error + "\r\nPlease contact Support");
                        EventLog.WriteEntry("DATSListenerService", "An Error Occurred during initialisation\r\n" + error + "\r\nPlease contact Support", EventLogEntryType.Error);
                        this.Stop();
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "PerformAsynStart()", ex);
                EventLog.WriteEntry("DATSListenerService", ex.Message, EventLogEntryType.Error);
            }

        }


        /// <summary>
        /// Stop this service.
        /// </summary>
        protected override void OnStop()
        {
            try
            {
                if (_listener != null)
                {
                    _listener.Stop();
                    _listener.Dispose();

                    int timeout = 4000;
                    while ((timeout > 0) && (!_listener.Stopped))
                    {
                        timeout -= 10;
                        Thread.Sleep(10);
                    }
                    _listener = null;
                }

            }
            catch (System.Exception ex)
            {
                EventLog.WriteEntry("DATSListenerService", ex.Message, EventLogEntryType.Error, 0, 0, null);
            }
        }
       
    }

}

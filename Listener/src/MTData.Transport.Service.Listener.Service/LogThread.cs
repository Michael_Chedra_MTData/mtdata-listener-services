using System;
using System.Threading;
using System.Collections;
using System.IO;
using System.Configuration;

namespace MTData.Transport.Service.Listener.Service
{
	/// <summary>
	/// Summary description for cLogThread.
	/// </summary>
	public class LogThread
	{
		#region Private Vars
		private Queue oQueue = null;
		private Thread tLogThread = null;
		private bool bLogThreadActive = false;
		private string sCurrentLogFile  = "";
		private string sLogDir  = "";
		private string sLogPrefix  = "";
		private string sLogFilename  = "";
		private string sLogExtenstion  = "";

		private bool bLogMsgs = true;
		#endregion

		#region Constructors and Threads
		public LogThread()
		{
			oQueue = new Queue();
			tLogThread = new Thread(new ThreadStart(LogFileQueue));
			tLogThread.Name = "cLogThread.WriteToLogThread";

			if (ConfigurationManager.AppSettings["LogConsoleMsgsToFile"] == "true")
				bLogMsgs = true;
			else
				bLogMsgs = false;

            sLogDir = ConfigurationManager.AppSettings["LogFileDir"];
            sLogPrefix = ConfigurationManager.AppSettings["LogFileNamePrefixFormat"];
            sLogFilename = ConfigurationManager.AppSettings["LogFileName"];
            sLogExtenstion = ConfigurationManager.AppSettings["LogFileExtension"];

			if (bLogMsgs)
			{
				tLogThread.Start();
				bLogThreadActive = true;
			}
			else
			{
				bLogThreadActive = false;
			}
		}

		public void Start()
		{
			if (bLogThreadActive) return;

			if (tLogThread == null)
			{
				tLogThread = new Thread(new ThreadStart(LogFileQueue));
				tLogThread.Name = "cLogThread.WriteToLogThread";
			}
			bLogThreadActive = true;
			oQueue.Clear();
			tLogThread.Start();
		}

		public void Stop()
		{
			if (!bLogThreadActive) return;
			
			bLogThreadActive = false;
			oQueue.Clear();
		}

		public void Dispose()
		{
			bLogThreadActive = false;
			if (oQueue != null)
			{
				oQueue.Clear();
				oQueue = null;
			}
			GC.Collect();
		}

		private void LogFileQueue()
		{

			int count = 0;
			while(bLogThreadActive)
			{
				lock(oQueue)
					count = oQueue.Count;

				if (count > 0)
				{
					//	If we encounter an error weriting to the log file, we cannot 
					//	stop logging.. we have to keep trying
					try
					{
						string[] messages = new string[count];

						lock(oQueue)
						{
							for(int loop = 0; loop < count; loop++)
								messages[loop] = (string) oQueue.Dequeue();
						}
						WriteToFile(messages);
						
					}
					catch(Exception ex)
					{
						Console.WriteLine("Error occurred writing to log file - {0} : {1}", ex.Message, ex.StackTrace);
					}

					Thread.Sleep(1);
				}
				else
				{
					Thread.Sleep(10);
				}
			}

			try
			{
				tLogThread.Abort();
			}
			catch(System.Exception)
			{
			}
		}

		#endregion

		#region Public Interface
		public void LogErrorMsg(System.Exception ex)
		{
			if (!bLogThreadActive) return;
			string sMsg = "";

			sMsg = System.DateTime.Now.ToString() + " : " + "Error Message : \n      Message : " + ex.Message + "\n      Source : " + ex.Source + "\n      Stack : " + ex.StackTrace;
			lock(oQueue)
			{
				oQueue.Enqueue(sMsg);
			}
		}

		public void LogMsg(string sMsg)
		{
			if (!bLogThreadActive) return;
			sMsg = sMsg.Trim();
			if (sMsg.Length > 0)
			{
				sMsg = System.DateTime.Now.ToString() + " : " + sMsg;
				lock(oQueue)
				{
					oQueue.Enqueue(sMsg);
				}
			}
		}
		#endregion
		/*
 
		 */
		#region WriteToConsole Functions
		private void WriteToFile(string[] messages)
		{
			if (messages.Length == 0)
				return;

			string sFilename = "";
			string sDir = "";
			string sTemp = "";
			System.IO.StreamWriter oFS = null;

			for(int loop = 0; loop < messages.Length; loop++)
				Console.Write(messages[loop] + "\n");

			// If the configuration is setup to log to file.

			sDir = sLogDir;
			sDir = sDir.Trim();
			if (sDir.Substring(sDir.Length - 1, 1) != "\\")
				sDir += "\\";

			if (!System.IO.Directory.Exists(sDir))
			{
				System.IO.Directory.CreateDirectory(sDir);
			}
			// Create the file name with todays date.
			switch (sLogPrefix)
			{
				case "DMY":
					sFilename = System.DateTime.Now.Day + "-" + System.DateTime.Now.Month + "-" + System.DateTime.Now.Year + " - " +sLogFilename + "." + sLogExtenstion;
					break;
				case "DYM":
					sFilename = System.DateTime.Now.Day + "-" + System.DateTime.Now.Year + "-" + System.DateTime.Now.Month + " - " +sLogFilename + "." + sLogExtenstion;
					break;
				case "MDY":
					sFilename = System.DateTime.Now.Month + "-" + System.DateTime.Now.Day + "-" + System.DateTime.Now.Year + " - " + sLogFilename + "." + sLogExtenstion;
					break;
				case "MYD":
					sFilename = System.DateTime.Now.Month + "-" + System.DateTime.Now.Year + "-" + System.DateTime.Now.Day + " - " + sLogFilename + "." + sLogExtenstion;
					break;
				case "YMD":
					sFilename = System.DateTime.Now.Year + "-" + System.DateTime.Now.Month + "-" + System.DateTime.Now.Day + " - " + sLogFilename + "." + sLogExtenstion;
					break;
				case "YDM":
					sFilename = System.DateTime.Now.Year + "-" + System.DateTime.Now.Day + "-" + System.DateTime.Now.Month + " - " + sLogFilename + "." + sLogExtenstion;
					break;
				default:
					sFilename = System.DateTime.Now.Day + "-" + System.DateTime.Now.Month + "-" + System.DateTime.Now.Year + " - " + sLogFilename + "." + sLogExtenstion;
					break;
			}
			sFilename = sDir + sFilename;

			// If the file name has changed or logging is starting for the first time..
			bool open = false;
			try
			{
				if (sCurrentLogFile != sFilename)
				{
					sCurrentLogFile = sFilename;

					// Send out a log message to say when logging started.
						
					sTemp = System.DateTime.Now.ToString() + " : " + "Log Serivces Started - Logging to " + sCurrentLogFile;
					Console.Write(sTemp + "\n");

					oFS = System.IO.File.AppendText(sCurrentLogFile);
					open = true;
					oFS.WriteLine(sTemp);
				}

				if (System.IO.File.Exists(sCurrentLogFile))
				{
					if (!open)
					{
						oFS = System.IO.File.AppendText(sCurrentLogFile);
						open = true;
					}

					for(int loop = 0; loop < messages.Length; loop++)
						oFS.WriteLine(messages[loop]);
				}
			}
			finally
			{
				if (open)
					oFS.Close();
			}

		}
		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using log4net;
using MTData.Common.Utilities;

namespace MTData.Transport.Service.Listener.Service
{
    static class Program
    {


        // The main entry point for the process
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            System.ServiceProcess.ServiceBase[] ServicesToRun;
            var log = LogManager.GetLogger(typeof(Program));
            string emailServiceUrl = ConfigurationManager.AppSettings["EmailServiceUrl"];
            var emailHelper = new EmailHelper(new EmailClient(emailServiceUrl));
            ServicesToRun = new System.ServiceProcess.ServiceBase[]
            {
                new DATSListenerService(emailHelper)
            };

            System.ServiceProcess.ServiceBase.Run(ServicesToRun);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = (Exception)e.ExceptionObject;
            string sMsg = "App Domain Error :\r\n" + ex.Message + "\r\n\r\nSource : \r\n" + ex.Source + "\r\n\r\nStack Trace : \r\n" + ex.StackTrace;
            EventLog el = new EventLog();
            el.Source = "MTData Listener Service";
            el.WriteEntry(sMsg, System.Diagnostics.EventLogEntryType.Error);
            el.Close();
        }


    }
}

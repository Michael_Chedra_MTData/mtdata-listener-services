﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;

namespace MTData.Transport.Service.Listener.CommandUtil
{
    class Program
    {
        static void Main(string[] args)
        {
            string command = null;
            List<string> units = new List<string>();
            string hardwareType = null;
            List<string> cmdParams = new List<string>();

            try
            {
                for (int i = 0; i < args.Length; i++)
                {
                    string arg = args[i].ToLower();
                    if (arg == "-help")
                    {
                        Console.Out.WriteLine("MTData.Transport.Service.Listener.CommandUtil -c cmd -u unitsFile -h hardwaretype -o options");
                    }
                    else if (arg == "-c")
                    {
                        command = args[++i];
                    }
                    else if (arg == "-u")
                    {
                        string unitsFile = args[++i];
                        if (File.Exists(unitsFile))
                        {
                            StreamReader file = new StreamReader(unitsFile);
                            string line = null;
                            while ((line = file.ReadLine()) != null)
                            {
                                units.Add(line.Trim());
                            }
                            file.Close();
                        }
                        else
                        {
                            throw new Exception("Units file does not exist");
                        }
                    }
                    else if (arg == "-h")
                    {
                        hardwareType = args[++i];
                    }
                    else if (arg == "-o")
                    {
                        while (i < args.Length - 1)
                        {
                            cmdParams.Add(args[++i]);
                        }
                    }
                }
                //check if units or hardware type defined
                if (units.Count == 0 && string.IsNullOrEmpty(hardwareType))
                {
                    throw new Exception("no Units or hardware type defined");
                }
            }
            catch (Exception exp)
            {
                Console.Error.WriteLine("Error reading arguments - {0}", exp.Message);
                Console.ReadKey();
                return;
            }

            List<Listener> listeners = (List<Listener>)System.Configuration.ConfigurationManager.GetSection("Listeners");

            listeners.ForEach(l =>
            {
                try
                {
                    l.Login();
                    Console.Out.WriteLine("Server {0} - Logged in", l.Name);
                    l.Command(command, units, hardwareType, cmdParams);
                }
                catch (Exception exp)
                {
                    Console.Error.WriteLine("Error on Server {0} - {1}", l.Name, exp.Message);
                }
            });
            //Console.ReadKey();
        }
    }
}

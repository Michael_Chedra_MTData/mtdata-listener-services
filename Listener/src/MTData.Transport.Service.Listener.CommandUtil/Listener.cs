﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Xml;
using MTData.Transport.Service.Listener.CommandUtil.ListenerService;
using MTData.Common.Crypto;
using System.Configuration;

namespace MTData.Transport.Service.Listener.CommandUtil
{
    public class Listener : IConfigurationSectionHandler
    {
        private GatewayServiceDefinitionClient _service;
        private Credentials _appCred;

        public string Name { get; set; }
        public string Address { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public Listener() { }

        public void Login()
        {
            TextEncryption encryptor = new TextEncryption();

            EndpointAddress address = new EndpointAddress(Address);
            if (Address.Contains("https"))
            {
                _service = new GatewayServiceDefinitionClient("BasicHttpsBinding_IGatewayServiceDefinition", address);
            }
            else
            {
                _service = new GatewayServiceDefinitionClient("BasicHttpBinding_IGatewayServiceDefinition", address);
            }

            int appVersion = 0;
            try
            {
                appVersion = _service.AppVersion();
            }
            catch (ActionNotSupportedException)
            {
                // Version 7 or below uses old encryption method
            }
            catch (Exception)
            {
            }

            _appCred = new Credentials() { Username = UserName };
            // AppVersion is a new web method exists from Version 8, so version 7 or below it will be null
            if (appVersion <= 7)
            {
                _appCred.Password = encryptor.Encrypt(encryptor.EncryptBase64(Password));
            }
            else
            {
                _appCred.Password = encryptor.Encrypt(encryptor.EncryptSaltBase64(UserName, Password));
            }

            try
            {
                int userID = _service.CheckLogin(_appCred);
                if (userID > 0)
                {
                    _appCred.UserID = userID;
                }
                else
                {
                    throw new Exception("Login Error - Username/Password incorrect");
                }
            }
            catch (System.ServiceModel.EndpointNotFoundException)
            {
                throw new Exception("Endpoint Error - Endpoint configuration incorrect or server offline");
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Login Error - {0}", ex.Message));
            }

        }

        public void Command(string command, List<string> units, string hardwareType, List<string> cmdParams)
        {
            //get list of vehicles
            List<Vehicle> vehicles = null;
            var fv = _service.GetFleetVehicles(_appCred);
            if (units.Count > 0)
            {
                vehicles = fv.Where(a => units.Contains(a.SerialNumber)).ToList();
            }
            else
            {
                var ht = _service.GetAssetHardwareTypes(_appCred);
                var hardware = ht.FirstOrDefault(h => h.Description == hardwareType);
                vehicles = fv.Where(a => a.AssetTypeID == hardware.ID).ToList();
            }

            if (command == "Download")
            {
                CreateDownloads(command, cmdParams, vehicles);
            }
            else
            {
                CreateCommands(command, cmdParams, vehicles);
            }
        }

        private void CreateDownloads(string command, List<string> cmdParams, List<Vehicle> vehicles)
        {
            int abort = 0;
            bool relogin = false;
            int definitionId = -1;
            string fileName = "unknown";
            for (int i = 0; i < cmdParams.Count; i++)
            {
                string arg = cmdParams[i].ToLower();
                if (arg == "abort")
                {
                    abort = Convert.ToInt32(cmdParams[++i]);
                }
                else if (arg == "relogin")
                {
                    relogin = Convert.ToBoolean(cmdParams[++i]);
                }
                else if (arg == "file")
                {
                    fileName = cmdParams[++i];
                    var dts = _service.GetDownloadTypes(_appCred);
                    foreach(var dt in dts)
                    {
                        var dfs = _service.GetFileImagesForDownloadType(dt.ID, _appCred);
                        //use contains as description has version numbers on the end
                        var df = dfs.FirstOrDefault(dfi=> dfi.Description.Contains(fileName) && dfi.AssetTypeID == vehicles[0].AssetTypeID);
                        if (df != null)
                        {
                            definitionId = df.ID;
                        }
                    }
                }
            }

            if (definitionId < 0)
            {
                Console.Error.WriteLine("Download file {0} not found", fileName);
                return;
            }

            QueuedDownload d = new QueuedDownload()
            {
                DefinitionID = definitionId,
                StartDate = DateTime.UtcNow,
                ExpiryDate = DateTime.UtcNow.AddHours(1),
                ForceLogout = false,
                ForceUpdate = false,
                ContinueWithoutRelogin = relogin,
                AbortTimeout = abort
            };

            vehicles.ForEach(v =>
            {
                d.FleetID = v.FleetID;
                d.VehicleId = v.VehicleID;
                d.TimeCreated = DateTime.Now.ToUniversalTime();
                int response = _service.SendDownloadToQueue(d, _appCred);
                if (response != 0)
                {
                    Console.Error.WriteLine("Download {0} not sent to Fleet {1} Vehicle {2}", command, v.FleetID, v.VehicleID);
                }
                else
                {
                    Console.Out.WriteLine("Download {0} sent to Fleet {1} Vehicle {2}", command, v.FleetID, v.VehicleID);
                }
            });
        }


        private void CreateCommands(string command, List<string> cmdParams, List<Vehicle> vehicles)
        { 
            QueuedMessage qMessage = new QueuedMessage()
            {
                StartDate = DateTime.UtcNow,
                ExpiryDate = DateTime.UtcNow.AddHours(1)
            };
            switch (command.ToLower())
            {
                case "reset":
                    qMessage.MessageTypeId = ListenerMessageTypes.Reset;
                    break;
                case "flushreset":
                    qMessage.MessageTypeId = ListenerMessageTypes.FlushThenReset;
                    break;
                case "flush":
                    qMessage.MessageTypeId = ListenerMessageTypes.Flush;
                    break;
                case "gforce":
                    qMessage.MessageTypeId = ListenerMessageTypes.GForceAutoCalibrate;
                    break;
                case "output1on":
                    qMessage.MessageTypeId = ListenerMessageTypes.Output1On;
                    break;
                case "output1off":
                    qMessage.MessageTypeId = ListenerMessageTypes.Output1Off;
                    break;
                case "output2on":
                    qMessage.MessageTypeId = ListenerMessageTypes.Output2On;
                    break;
                case "output2off":
                    qMessage.MessageTypeId = ListenerMessageTypes.Output2Off;
                    break;
                case "ab1":
                    qMessage.MessageTypeId = ListenerMessageTypes.AccidentBuffer1;
                    break;
                case "ab2":
                    qMessage.MessageTypeId = ListenerMessageTypes.AccidentBuffer2;
                    break;
                case "ab3":
                    qMessage.MessageTypeId = ListenerMessageTypes.AccidentBuffer3;
                    break;
                case "ab4":
                    qMessage.MessageTypeId = ListenerMessageTypes.AccidentBuffer4;
                    break;
                case "changeip":
                    qMessage.MessageTypeId = ListenerMessageTypes.ChangeIPAddress;
                    qMessage.ExtraData = CreateNetworkSettings(cmdParams);
                    break;
                case "iap":
                    qMessage.MessageTypeId = ListenerMessageTypes.IAPDataBlocks;
                    qMessage.ExtraData = CreateIAPDataBlockSettings(cmdParams);
                    break;
            }

            vehicles.ForEach(v=>
            {
                qMessage.FleetId = v.FleetID;
                qMessage.VehicleId = v.VehicleID;
                qMessage.CreatedDate = DateTime.Now.ToUniversalTime();
                int response = _service.SendMessageToQueue(qMessage, _appCred);
                if (response != 0)
                {
                    Console.Error.WriteLine("Command {0} not sent to Fleet {1} Vehicle {2}", command, v.FleetID, v.VehicleID);
                }
                else
                {
                    Console.Out.WriteLine("Command {0} sent to Fleet {1} Vehicle {2}", command, v.FleetID, v.VehicleID);
                }
            });

        }

        private string CreateNetworkSettings(List<string> cmdParams)
        {
            string hostNameChecked = "False";
            string hostIp = "";
            string hostPort = "3344";
            string secIp = "";
            string secPort = "3344";
            string pingIp = "";
            string pingRetries = "5";
            for (int i = 0; i < cmdParams.Count; i++)
            {
                string arg = cmdParams[i].ToLower();
                if (arg == "hostname")
                {
                    hostNameChecked = "True";
                }
                if (arg == "hostip")
                {
                    hostIp = cmdParams[++i];
                }
                if (arg == "hostport")
                {
                    hostPort = cmdParams[++i];
                }
                if (arg == "secport")
                {
                    secPort = cmdParams[++i];
                }
                if (arg == "secip")
                {
                    secIp = cmdParams[++i];
                }
                if (arg == "pingretries")
                {
                    pingRetries = cmdParams[++i];
                }
                if (arg == "pingip")
                {
                    pingIp = cmdParams[++i];
                }
            }
            XmlDocument doc = new XmlDocument();
            XmlElement extraData = doc.CreateElement("extraData");
            extraData.SetAttribute("hostName", hostNameChecked);
            extraData.SetAttribute("hostIP", hostIp);
            extraData.SetAttribute("ipPort", hostPort);
            extraData.SetAttribute("secIPPort", secPort);
            extraData.SetAttribute("secIP", secIp);
            extraData.SetAttribute("pingRetries", pingRetries);
            extraData.SetAttribute("pingIP", pingIp);
            doc.AppendChild(extraData);
            return doc.OuterXml;
        }

        private string CreateIAPDataBlockSettings(List<string> cmdParams)
        {
            string position = "0";
            string speed = "0";
            string block = "0";
            for (int i = 0; i < cmdParams.Count; i++)
            {
                string arg = cmdParams[i].ToLower();
                if (arg == "position")
                {
                    position = cmdParams[++i];
                }
                else if (arg == "speed")
                {
                    speed = cmdParams[++i];
                }
                else if (arg == "block")
                {
                    block = cmdParams[++i];
                }
            }
            XmlDocument doc = new XmlDocument();
            XmlElement extraData = doc.CreateElement("extraData");
            extraData.SetAttribute("position", position);
            extraData.SetAttribute("speed", speed);
            extraData.SetAttribute("block", block);
            doc.AppendChild(extraData);
            return doc.OuterXml;
        }

        public object Create(object parent, object configContext, XmlNode section)
        {
            List<Listener> listeners = new List<Listener>();

            XmlNodeList nodes = section.SelectNodes("Listener");
            foreach (XmlNode n in nodes)
            {
                listeners.Add(new Listener()
                {
                    Name = n.Attributes.GetNamedItem("Name").Value,
                    UserName = n.Attributes.GetNamedItem("UserName").Value,
                    Password = n.Attributes.GetNamedItem("Password").Value,
                    Address = n.Attributes.GetNamedItem("Address").Value
                });
            }

            return listeners;

        }
    }
}
